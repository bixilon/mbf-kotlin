package de.bixilon.mbf.data.type.array

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object MixedArrayType : MbfDataType<Any?> {

    override fun read(reader: MBFBinaryReader): Any {
        val length = reader.readLength()
        if (length == 0) return emptyArray<Any>()

        val array: Array<Any?> = arrayOfNulls(length)

        for (i in array.indices) {
            array[i] = reader.readMBFEntry() ?: continue
        }
        return array
    }

    override fun write(writer: MBFBinaryWriter, value: Any?) {
        check(value is Array<*>)
        writer.writeLength(value.size)
        for (entry in value) {
            writer.writeMBF(entry)
        }
    }
}
