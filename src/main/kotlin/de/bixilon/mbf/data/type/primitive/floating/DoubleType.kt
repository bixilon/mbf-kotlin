package de.bixilon.mbf.data.type.primitive.floating

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object DoubleType : MbfDataType<Double> {

    override fun read(reader: MBFBinaryReader) = reader.readDouble()
    override fun write(writer: MBFBinaryWriter, value: Double) = writer.writeDouble(value)
}
