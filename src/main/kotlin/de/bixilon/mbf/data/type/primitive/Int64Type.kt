package de.bixilon.mbf.data.type.primitive

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object Int64Type : MbfDataType<Long> {

    override fun read(reader: MBFBinaryReader) = reader.readLong()
    override fun write(writer: MBFBinaryWriter, value: Long) = writer.writeLong(value)
}
