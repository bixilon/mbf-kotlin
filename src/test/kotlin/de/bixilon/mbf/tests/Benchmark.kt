/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

/*
package de.bixilon.mbf.tests

import de.bixilon.mbf.MBFBinaryReader
import java.io.ByteArrayInputStream
import java.io.FileInputStream
import kotlin.system.measureNanoTime
import kotlin.test.Test

internal class Benchmark {

    @Test
    fun test() {
        val data = FileInputStream("/home/moritz/git/gitlab.bixilon.de/bixilon/pixlyzer-data/version/1.20.1/all.mbf").readAllBytes()


        val time = measureNanoTime {
            for (i in 0 until 100) {
                MBFBinaryReader(ByteArrayInputStream(data)).readMBF()
            }
        } / 100

        println("Took ${time}ns, ${time / 1_000}µs, ${time / 1_000_000}ms")
    }
}
*/
