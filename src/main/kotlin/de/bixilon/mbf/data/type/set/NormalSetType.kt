package de.bixilon.mbf.data.type.set

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.MBFUtil.generics
import de.bixilon.mbf.data.MbfDataType

object NormalSetType : MbfDataType<Any?> {

    override fun read(reader: MBFBinaryReader): Set<*> {
        val length = reader.readLength()
        val type = reader.readMBFDataType()
        if (length == 0) return emptySet<Any>()

        val set: MutableSet<Any> = HashSet(length, 1.0f)

        for (i in 0 until length) {
            set += type.type.read(reader) ?: continue
        }
        return set
    }

    override fun write(writer: MBFBinaryWriter, value: Any?) {
        check(value is Set<*>)
        writer.writeLength(value.size)
        val type = writer.getDataTypeClass(value.generics[0])
        writer.writeByte(type.ordinal)

        for (entry in value) {
            (type.type as MbfDataType<Any?>).write(writer, entry)
        }
    }
}
