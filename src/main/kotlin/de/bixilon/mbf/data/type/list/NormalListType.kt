package de.bixilon.mbf.data.type.list

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.MBFUtil.generics
import de.bixilon.mbf.data.MbfDataType

object NormalListType : MbfDataType<Any?> {

    override fun read(reader: MBFBinaryReader): Any {
        val length = reader.readLength()
        val type = reader.readMBFDataType()
        if (length == 0) return emptyList<Any>()

        val list: MutableList<Any> = ArrayList(length)

        for (i in 0 until length) {
            list += type.type.read(reader) ?: continue
        }
        return list
    }

    override fun write(writer: MBFBinaryWriter, value: Any?) {
        check(value is List<*>)
        writer.writeLength(value.size)
        val type = writer.getDataTypeClass(value.generics[0])
        writer.writeByte(type.ordinal)
        for (entry in value) {
            (type.type as MbfDataType<Any?>).write(writer, entry)
        }
    }
}
