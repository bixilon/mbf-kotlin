package de.bixilon.mbf.data.type.primitive

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object Int32Type : MbfDataType<Int> {

    override fun read(reader: MBFBinaryReader) = reader.readInt()
    override fun write(writer: MBFBinaryWriter, value: Int) = writer.writeInt(value)
}
