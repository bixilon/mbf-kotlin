package de.bixilon.mbf.data.type.primitive

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object VarLongType : MbfDataType<Long> {

    override fun read(reader: MBFBinaryReader) = reader.readVarLong()
    override fun write(writer: MBFBinaryWriter, value: Long) = writer.writeVarLong(value)
}
