package de.bixilon.mbf.data.type.list

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object MixedListType : MbfDataType<Any?> {

    override fun read(reader: MBFBinaryReader): Any {
        val length = reader.readLength()
        if (length == 0) return emptyList<Any>()

        val list: MutableList<Any> = ArrayList(length)
        for (i in 0 until length) {
            list += reader.readMBFEntry() ?: continue
        }
        return list
    }

    override fun write(writer: MBFBinaryWriter, value: Any?) {
        check(value is List<*>)
        writer.writeLength(value.size)
        for (entry in value) {
            writer.writeMBF(entry)
        }
    }
}
