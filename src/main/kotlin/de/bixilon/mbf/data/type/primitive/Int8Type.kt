package de.bixilon.mbf.data.type.primitive

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object Int8Type : MbfDataType<Byte> {

    override fun read(reader: MBFBinaryReader) = reader.readByte()
    override fun write(writer: MBFBinaryWriter, value: Byte) = writer.writeByte(value)
}
