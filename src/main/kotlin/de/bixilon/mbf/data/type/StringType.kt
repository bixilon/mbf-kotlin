package de.bixilon.mbf.data.type

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object StringType : MbfDataType<String> {

    override fun read(reader: MBFBinaryReader) = reader.readString()
    override fun write(writer: MBFBinaryWriter, value: String) = writer.writeString(value)
}
