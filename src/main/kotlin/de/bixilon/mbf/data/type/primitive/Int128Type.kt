package de.bixilon.mbf.data.type.primitive

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType
import java.util.*

object Int128Type : MbfDataType<UUID> {

    override fun read(reader: MBFBinaryReader) = reader.readUUID()
    override fun write(writer: MBFBinaryWriter, value: UUID) = writer.writeUUID(value)
}
