package de.bixilon.mbf.data.type.map

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.MBFUtil.generics
import de.bixilon.mbf.data.MbfDataType

object NormalMapType : MbfDataType<Any?> {

    override fun read(reader: MBFBinaryReader): Any {
        val length = reader.readLength()
        val keyType = reader.readMBFDataType()
        val valueType = reader.readMBFDataType()
        if (length == 0) return emptyMap<Any, Any>()

        val map = HashMap<Any, Any>(length, 1.0f)

        for (i in 0 until length) {
            val key = keyType.type.read(reader)
            val value = valueType.type.read(reader)
            if (key == null || value == null) {
                continue
            }
            map[key] = value
        }
        return map
    }

    override fun write(writer: MBFBinaryWriter, value: Any?) {
        check(value is Map<*, *>)
        writer.writeLength(value.size)
        val keyType = writer.getDataTypeClass(value.generics[0])
        val valueType = writer.getDataTypeClass(value.generics[1])
        writer.writeByte(keyType.ordinal)
        writer.writeByte(valueType.ordinal)
        for ((key, value) in value) {
            (keyType.type as MbfDataType<Any?>).write(writer, key)
            (valueType.type as MbfDataType<Any?>).write(writer, value)
        }
    }
}
