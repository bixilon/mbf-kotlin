/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.mbf

data class MBFDataInfo(
    /**
     * Compression type, defaults to ZSTD
     */
    var compression: MBFCompressionTypes = MBFCompressionTypes.ZSTD,
    @Deprecated("Unsupported")
    var encryption: Boolean = false,
    /**
     * If true, the reader/writer uses a VarInt as length prefix instead of an int
     */
    var variableLengthPrefix: Boolean = true,
    /**
     * If true, the writer writes a VarInt instead of an int (or long)
     */
    var preferVariableTypes: Boolean = true,
    /**
     * Current format version, defaults to 0
     */
    var version: Int = 0,
    /**
     * If compression is not NONE, the writer uses this compression level
     */
    var compressionLevel: Int = 3,
)
