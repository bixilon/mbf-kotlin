package de.bixilon.mbf.data.type.primitive

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object BooleanType : MbfDataType<Boolean> {

    override fun read(reader: MBFBinaryReader) = reader.readBoolean()
    override fun write(writer: MBFBinaryWriter, value: Boolean) = writer.writeBoolean(value)
}
