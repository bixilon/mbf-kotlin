package de.bixilon.mbf.data.type.primitive.floating

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object FloatType : MbfDataType<Float> {

    override fun read(reader: MBFBinaryReader) = reader.readFloat()
    override fun write(writer: MBFBinaryWriter, value: Float) = writer.writeFloat(value)
}
