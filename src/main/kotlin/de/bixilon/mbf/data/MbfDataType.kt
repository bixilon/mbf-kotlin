package de.bixilon.mbf.data

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter

interface MbfDataType<T> {

    fun read(reader: MBFBinaryReader): T
    fun write(writer: MBFBinaryWriter, value: T)
}
