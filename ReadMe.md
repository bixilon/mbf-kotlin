# Moritz's Binary Format Kotlin

A kotlin implementation for [MBF](https://gitlab.bixilon.de/bixilon/mbf-specification).

## How to get?

Maven central

```xml
<dependency>
    <groupId>de.bixilon</groupId>
    <artifactId>mbf-kotlin</artifactId>
    <version>1.0.3</version>
</dependency>
```

## Examples

### Reading

```kotlin
import de.bixilon.mbf.MBFBinaryReader
import java.io.ByteArrayInputStream

val reader = MBFBinaryReader(ByteArrayInputStream(data)).readMBF()
```

### Writing

```kotlin
import de.bixilon.mbf.MBFData
import de.bixilon.mbf.MBFBinaryWriter
import java.io.ByteArrayOutputStream

val outputStream = ByteArrayOutputStream()
MBFBinaryWriter(outputStream).writeMBF(MBFData(data = data))

val byteArray = outputStream.toByteArray()
```
