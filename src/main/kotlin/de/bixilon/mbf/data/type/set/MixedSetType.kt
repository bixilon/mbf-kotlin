package de.bixilon.mbf.data.type.set

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object MixedSetType : MbfDataType<Any?> {

    override fun read(reader: MBFBinaryReader): Set<*> {
        val length = reader.readLength()
        if (length == 0) return emptySet<Any>()

        val set: MutableSet<Any> = HashSet(length, 1.0f)

        for (i in 0 until length) {
            set += reader.readMBFEntry() ?: continue
        }
        return set
    }

    override fun write(writer: MBFBinaryWriter, value: Any?) {
        check(value is Set<*>)
        writer.writeLength(value.size)
        for (entry in value) {
            writer.writeMBF(entry)
        }
    }
}
