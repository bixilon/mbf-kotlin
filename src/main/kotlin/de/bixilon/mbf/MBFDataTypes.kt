/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.mbf

import de.bixilon.mbf.data.MbfDataType
import de.bixilon.mbf.data.NullType
import de.bixilon.mbf.data.type.StringType
import de.bixilon.mbf.data.type.array.MixedArrayType
import de.bixilon.mbf.data.type.array.NormalArrayType
import de.bixilon.mbf.data.type.list.MixedListType
import de.bixilon.mbf.data.type.list.NormalListType
import de.bixilon.mbf.data.type.map.MixedMapType
import de.bixilon.mbf.data.type.map.NormalMapType
import de.bixilon.mbf.data.type.primitive.*
import de.bixilon.mbf.data.type.primitive.floating.DoubleType
import de.bixilon.mbf.data.type.primitive.floating.FloatType
import de.bixilon.mbf.data.type.set.MixedSetType
import de.bixilon.mbf.data.type.set.NormalSetType

enum class MBFDataTypes(val type: MbfDataType<*>) {
    NULL(NullType),
    BOOLEAN(BooleanType),
    INT8(Int8Type),
    INT16(Int16Type),
    INT32(Int32Type),
    INT64(Int64Type),
    INT128(Int128Type),
    FLOAT(FloatType),
    DOUBLE(DoubleType),
    VAR_INT(VarIntType),

    STRING(StringType),

    NORMAL_ARRAY(NormalArrayType),
    MIXED_ARRAY(MixedArrayType),

    NORMAL_LIST(NormalListType),
    MIXED_LIST(MixedListType),

    NORMAL_SET(NormalSetType),
    MIXED_SET(MixedSetType),

    NORMAL_MAP(NormalMapType),
    MIXED_MAP(MixedMapType),

    VAR_LONG(VarLongType),
    ;

    companion object {
        val VALUES = values()
    }
}
