/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.mbf.tests

import de.bixilon.mbf.*
import org.junit.jupiter.api.Assertions
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import kotlin.test.Test
import kotlin.test.assertEquals

internal class AdvancedTests {
    private val recursiveExample = MBFData(
        dataInfo = MBFDataInfo(
            compression = MBFCompressionTypes.NONE,
            preferVariableTypes = false,
        ),
        data = mutableMapOf(
            "test" to 12,
            "test2" to mutableListOf(
                12,
                "12",
                12.0,
                12.0f,
            ),
            "test3" to mutableMapOf(
                12 to "12",
                "map" to mutableListOf(
                    "test",
                    12.0,
                    mutableMapOf(
                        "whatever" to "this",
                        "foo" to "bar",
                        12 to 12.0,
                        12.0 to 12.0f,
                    )
                )
            )
        )
    )

    private val complexExample = MBFData(
        dataInfo = MBFDataInfo(
            compression = MBFCompressionTypes.NONE,
            preferVariableTypes = false,
        ),
        data = mutableMapOf(
            "test" to mutableMapOf(
                "id" to 0,
                "test" to true,
                "test" to "test",
                "data" to mutableMapOf(
                    "test" to 8,
                    "test2" to 8,
                    "test3" to 10,
                ),
                "attributes" to mutableMapOf(
                    "first" to 1.0,
                    "second" to 2.0,
                    "500th" to 500.0,
                ),
                "last" to "one",
            ),
            "longs" to mutableListOf(
                0L,
                1268743L,
                Long.MIN_VALUE,
                Long.MAX_VALUE,
            )
        )
    )

    private val recursiveBinary = byteArrayOf(
        77, 66, 70, 0, 8, 18, 3, 10, 4, 116, 101, 115, 116, 4, 0, 0, 0, 12, 10, 5, 116, 101, 115, 116, 50, 14, 4, 4, 0, 0, 0, 12, 10, 2, 49, 50, 8, 64,
        40, 0, 0, 0, 0, 0, 0, 7, 65, 64, 0, 0, 10, 5, 116, 101, 115, 116, 51, 18, 2, 4, 0, 0, 0, 12, 10, 2, 49, 50, 10, 3, 109, 97, 112, 14, 3, 10, 4,
        116, 101, 115, 116, 8, 64, 40, 0, 0, 0, 0, 0, 0, 18, 4, 10, 8, 119, 104, 97, 116, 101, 118, 101, 114, 10, 4, 116, 104, 105, 115, 10, 3, 102, 111,
        111, 10, 3, 98, 97, 114, 4, 0, 0, 0, 12, 8, 64, 40, 0, 0, 0, 0, 0, 0, 8, 64, 40, 0, 0, 0, 0, 0, 0, 7, 65, 64, 0, 0,
    )
    private val complexBinary = byteArrayOf(
        77, 66, 70, 0, 8, 18, 2, 10, 4, 116, 101, 115, 116, 18, 5, 10, 2, 105, 100, 4, 0, 0, 0, 0, 10, 4, 116, 101, 115, 116, 10, 4, 116, 101, 115, 116,
        10, 4, 100, 97, 116, 97, 17, 3, 10, 4, 4, 116, 101, 115, 116, 0, 0, 0, 8, 5, 116, 101, 115, 116, 50, 0, 0, 0, 8, 5, 116, 101, 115, 116, 51, 0, 0,
        0, 10, 10, 10, 97, 116, 116, 114, 105, 98, 117, 116, 101, 115, 17, 3, 10, 8, 5, 102, 105, 114, 115, 116, 63, -16, 0, 0, 0, 0, 0, 0, 6, 115, 101,
        99, 111, 110, 100, 64, 0, 0, 0, 0, 0, 0, 0, 5, 53, 48, 48, 116, 104, 64, 127, 64, 0, 0, 0, 0, 0, 10, 4, 108, 97, 115, 116, 10, 3, 111, 110, 101,
        10, 5, 108, 111, 110, 103, 115, 13, 4, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19, 92, 7, -128, 0, 0, 0, 0, 0, 0, 0, 127, -1, -1, -1, -1, -1, -1, -1,
    )

    @Test
    fun testRecursiveWriting() {
        val outputStream = ByteArrayOutputStream()
        MBFBinaryWriter(outputStream).writeMBF(recursiveExample)

        // print(outputStream.toByteArray().contentToString())
        Assertions.assertArrayEquals(recursiveBinary, outputStream.toByteArray())
    }

    @Test
    fun testRecursiveReading() {
        val reader = MBFBinaryReader(ByteArrayInputStream(recursiveBinary))

        assertEquals<Any>(recursiveExample, reader.readMBF())
    }

    @Test
    fun testComplexWriting() {
        val outputStream = ByteArrayOutputStream()
        MBFBinaryWriter(outputStream).writeMBF(complexExample)

        Assertions.assertArrayEquals(complexBinary, outputStream.toByteArray())
    }

    @Test
    fun testComplexReading() {
        val reader = MBFBinaryReader(ByteArrayInputStream(complexBinary))

        assertEquals<Any>(complexExample, reader.readMBF())
    }
}
