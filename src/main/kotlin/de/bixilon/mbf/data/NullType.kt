package de.bixilon.mbf.data

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter

object NullType : MbfDataType<Any?> {

    override fun read(reader: MBFBinaryReader) = null
    override fun write(writer: MBFBinaryWriter, value: Any?) = Unit
}
