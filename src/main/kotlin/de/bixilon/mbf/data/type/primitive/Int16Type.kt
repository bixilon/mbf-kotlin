package de.bixilon.mbf.data.type.primitive

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object Int16Type : MbfDataType<Short> {

    override fun read(reader: MBFBinaryReader) = reader.readShort()
    override fun write(writer: MBFBinaryWriter, value: Short) = writer.writeShort(value)
}
