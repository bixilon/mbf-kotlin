package de.bixilon.mbf.data.type.array

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.MBFDataTypes
import de.bixilon.mbf.data.MbfDataType

object NormalArrayType : MbfDataType<Any?> {

    override fun read(reader: MBFBinaryReader): Any {
        val length = reader.readLength()

        return when (val type = reader.readMBFDataType()) {
            MBFDataTypes.NULL -> arrayOfNulls<Any?>(length)
            MBFDataTypes.BOOLEAN -> reader.readBooleanArray()
            MBFDataTypes.INT8 -> reader.readByteArray(length)
            MBFDataTypes.INT16 -> reader.readShortArray(length)
            MBFDataTypes.INT32 -> reader.readIntArray(length)
            MBFDataTypes.INT64 -> reader.readLongArray(length)
            MBFDataTypes.FLOAT -> reader.readFloatArray(length)
            MBFDataTypes.DOUBLE -> reader.readDoubleArray(length)
            MBFDataTypes.VAR_INT -> reader.readVarIntArray(length)
            MBFDataTypes.VAR_LONG -> reader.readVarLongArray(length)
            else -> reader.readArray(length) { type.type.read(reader) }
        }
    }

    override fun write(writer: MBFBinaryWriter, value: Any?) {
        when (value) {
            null -> {
                writer.writeLength(0)
                writer.writeByte(MBFDataTypes.NULL.ordinal)
            }

            is BooleanArray -> {
                writer.writeLength(value.size)
                writer.writeByte(MBFDataTypes.BOOLEAN.ordinal)
                writer.writeBooleanArray(value)
            }

            is ByteArray -> {
                writer.writeLength(value.size)
                writer.writeByte(MBFDataTypes.INT8.ordinal)
                writer.writeByteArray(value)
            }

            is ShortArray -> {
                writer.writeLength(value.size)
                writer.writeByte(MBFDataTypes.INT16.ordinal)
                writer.writeShortArray(value)
            }

            is IntArray -> {
                writer.writeLength(value.size)
                if (writer.preferVariableTypes) {
                    writer.writeByte(MBFDataTypes.VAR_INT.ordinal)
                    writer.writeVarIntArray(value)
                } else {
                    writer.writeByte(MBFDataTypes.INT32.ordinal)
                    writer.writeIntArray(value)
                }
            }

            is LongArray -> {
                writer.writeLength(value.size)
                if (writer.preferVariableTypes) {
                    writer.writeByte(MBFDataTypes.VAR_LONG.ordinal)
                    writer.writeVarLongArray(value)
                } else {
                    writer.writeByte(MBFDataTypes.INT64.ordinal)
                    writer.writeLongArray(value)
                }
            }

            is FloatArray -> {
                writer.writeLength(value.size)
                writer.writeByte(MBFDataTypes.FLOAT.ordinal)
                writer.writeFloatArray(value)
            }

            is DoubleArray -> {
                writer.writeLength(value.size)
                writer.writeByte(MBFDataTypes.DOUBLE.ordinal)
                writer.writeDoubleArray(value)
            }

            else -> {
                check(value is Array<*>)
                writer.writeLength(value.size)

                val type = writer.getDataTypeClass(value::class.java.componentType)
                writer.writeByte(type.ordinal)
                for (entry in value) {
                    (type.type as MbfDataType<Any?>).write(writer, entry)
                }
            }
        }
    }
}
