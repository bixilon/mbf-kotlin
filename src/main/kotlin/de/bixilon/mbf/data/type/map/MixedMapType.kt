package de.bixilon.mbf.data.type.map

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object MixedMapType : MbfDataType<Any?> {

    override fun read(reader: MBFBinaryReader): Any {
        val length = reader.readLength()
        if (length == 0) return emptyMap<Any, Any>()

        val map = HashMap<Any, Any>(length, 1.0f)

        for (i in 0 until length) {
            val key = reader.readMBFEntry()
            val value = reader.readMBFEntry()
            if (key == null || value == null) {
                continue
            }
            map[key] = value
        }
        return map
    }

    override fun write(writer: MBFBinaryWriter, value: Any?) {
        check(value is Map<*, *>)
        writer.writeLength(value.size)
        for ((key, value) in value) {
            writer.writeMBF(key)
            writer.writeMBF(value)
        }
    }
}
