package de.bixilon.mbf.data.type.primitive

import de.bixilon.mbf.MBFBinaryReader
import de.bixilon.mbf.MBFBinaryWriter
import de.bixilon.mbf.data.MbfDataType

object VarIntType : MbfDataType<Int> {

    override fun read(reader: MBFBinaryReader) = reader.readVarInt()
    override fun write(writer: MBFBinaryWriter, value: Int) = writer.writeVarInt(value)
}
